﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollumnsCtr : MonoBehaviour {
    private float move;
    private float minY;
    private float maxY;
    private float oldPosition;
    private GameObject obj;
    // Use this for initialization
    void Start()
    {
        obj = gameObject;
        oldPosition = 10;
        move = 2;
        minY = -1;
        maxY = 1;
    }

    // Update is called once per frame
    void Update()
    {
        obj.transform.Translate(new Vector3(-1 * Time.deltaTime * move, 0, 0));
    }
        
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Reset"))
        {
            obj.transform.position = new Vector3(oldPosition, Random.Range(minY, maxY + 1), 0);
        }
        
    }
}
