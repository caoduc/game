﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    bool isdead = false;
    bool flag = true;
    int gamepoint = 0;
    public Text txtpoint;
    public GameObject panelEndGame;
    public Text EndPoint;
    public Button btnRes;



	// Use this for initialization
	void Start () {
        Time.timeScale = 0;
        isdead = false;
        flag = true;
    }
	
	// Update is called once per frame
	void Update () {
        if(isdead)
        {
            if (Input.GetMouseButtonDown(0) && flag)
            {
                //Time.timeScale = 1;
                //isdead = false;
                ResGame();
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                Time.timeScale = 1;
            }
        }
		
	}

    public void GetPoint()
    {
        gamepoint++;
        txtpoint.text = "Point: " + gamepoint.ToString();
    }

    void ResGame()
    {
        SceneManager.LoadScene(0);
    }

    public void Restart()
    {
        ResGame();
    }

    public void EndGame()
    {
        flag = false;
        isdead = true;
        Time.timeScale = 0;
        panelEndGame.SetActive(true);
        EndPoint.text = "Final Point:\n" + gamepoint.ToString();
    }
}
