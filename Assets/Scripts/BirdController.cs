﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour {

    GameObject obj;
    GameObject gameController;
    Rigidbody2D rd;
    private float force;
    private Animator anim;
	// Use this for initialization
	void Start () {
        obj = gameObject;
        force = 200f;
        rd = GetComponent<Rigidbody2D>();
        anim = obj.GetComponent<Animator>();
        anim.SetFloat("flyPower", 0);
        anim.SetBool("isDead", false);

        if(gameController == null)
        {
            gameController = GameObject.FindGameObjectWithTag("GameController");
        }
	}
	
	// Update is called once per frame
	void Update () {
        if( this.transform.position.y<=4)
        {
            if (Input.GetMouseButtonDown(0))
            {
                rd.velocity = Vector2.zero;
                rd.AddForce(new Vector2(0, force));
                //obj.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, force));
                anim.SetFloat("flyPower", rd.velocity.y);
            }
        }
		
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        EndGame();
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        gameController.GetComponent<GameController>().GetPoint();
    }

    void EndGame()
    {
        anim.SetBool("isDead", true);
        gameController.GetComponent<GameController>().EndGame();
    }
}
