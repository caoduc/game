﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeft : MonoBehaviour {

	public float move;
    public float rageMove;
    private Vector3 oldPosition;
    private GameObject obj;
	// Use this for initialization
	void Start () {
        obj = gameObject;
        oldPosition = obj.transform.position;
        move = 2;
        rageMove = 18;
	}

	// Update is called once per frame
	void Update () {
		obj.transform.Translate (new Vector3(-1* Time.deltaTime*move,transform.position.y,0 ));

        if(Vector2.Distance(oldPosition, obj.transform.position)>rageMove)
        {
            obj.transform.position = oldPosition;
        }
	}
}
